import 'package:flutter/material.dart';
import 'package:future_builder_test/model/comment.dart';
import 'package:future_builder_test/views/widgets/comment_list_widget.dart';
import 'package:future_builder_test/views/widgets/comment_view_inherited_widget.dart';

class CommentView extends StatefulWidget {
  CommentView({Key key}) : super(key: key);

  CommentViewState createState() => new CommentViewState();
}

class CommentViewState extends State<CommentView> {
  List<Comment> filteredComments;
  TextEditingController searchController;
  String searchString;

  @override
  void initState() {
    super.initState();
    searchController = new TextEditingController();
    searchController.addListener(() => searchList(searchController.text));
    searchString = "";
  }

  @override
  void didChangeDependencies() {
    super.didChangeDependencies();
    filteredComments = CommentViewInheritedWidget.of(context).commentsList;
  }

  void searchList(String text) {
    setState(() {
      searchString = text;
    });
  }

  //rendere questo widget stateful per salvare la ricerca, e vedere come si comporta
  @override
  Widget build(BuildContext context) {
    return Column(
      mainAxisAlignment: MainAxisAlignment.spaceEvenly,
      crossAxisAlignment: CrossAxisAlignment.center,
      children: <Widget>[
        // create search widget here
        // CommentsSearchBar(),
        Row(
          mainAxisAlignment: MainAxisAlignment.spaceEvenly,
          mainAxisSize: MainAxisSize.max,
          children: <Widget>[
            Icon(Icons.search),
            Flexible(
              child: TextFormField(
                keyboardType: TextInputType.text,
                controller: searchController,
              ),
            ),
          ],
        ),
        CommentListWidget(
          commentsList: filteredComments
                  .where((Comment c) => (searchString.length > 3)
                      ? c.body.contains(searchString)
                      : true)
                  .toList(),
        ),
      ],
    );
  }

  @override
  void dispose() {
    searchController.dispose();
    super.dispose();
  }
}
