import 'package:flutter/material.dart';
import 'package:future_builder_test/model/comment.dart';
import 'package:future_builder_test/views/widgets/comment_view_inherited_widget.dart';

class CommentListWidget extends StatefulWidget {
  final List<Comment> commentsList;

  CommentListWidget({Key key, this.commentsList}) : super(key: key);

  @override
  CommentListWidgetState createState() => CommentListWidgetState();
}

class CommentListWidgetState extends State<CommentListWidget> {
  ScrollController _scrollController;
  final GlobalKey<RefreshIndicatorState> _refreshIndicatorKey =
      GlobalKey<RefreshIndicatorState>();

  @override
  void initState() {
    super.initState();
    _scrollController = ScrollController(initialScrollOffset: 0.0);
    _scrollController.addListener(() {
      double offset = _scrollController.offset;
      print("offset: $offset");
      if (offset < -50) _refreshIndicatorKey.currentState.show();
    });
  }

  @override
  Widget build(BuildContext context) {
    return Expanded(
      child: Column(
        mainAxisSize: MainAxisSize.min,
        mainAxisAlignment: MainAxisAlignment.center,
        children: <Widget>[
          Text("Risultati trovati: ${widget.commentsList.length}"),
          Expanded(
            child: RefreshIndicator(
              key: _refreshIndicatorKey,
              notificationPredicate: (ScrollNotification notification) => notification.depth == 1,
              onRefresh: () {
                CommentViewInheritedWidget.of(context).funcs["refreshList"]();
                return Future.delayed(Duration(microseconds: 1), () {});
              },
              child: ListView.builder(
                shrinkWrap: true,
                physics: BouncingScrollPhysics(),
                controller: _scrollController,
                itemCount: widget.commentsList.length,
                itemBuilder: (context, int index) {
                  return InkWell(
                    onLongPress: () => CommentViewInheritedWidget.of(context)
                            .funcs["showEditableDialog"](
                        context, widget.commentsList[index].id),
                    child: Card(
                      shape: Border.all(
                        color: Colors.black,
                      ),
                      elevation: 5,
                      child: Column(
                        children: <Widget>[
                          Text(
                            widget.commentsList[index].email,
                            softWrap: true,
                          ),
                          Divider(
                            height: 2,
                          ),
                          Text(
                            widget.commentsList[index].body,
                            softWrap: true,
                          ),
                        ],
                      ),
                    ),
                  );
                },
              ),
            ),
          ),
        ],
      ),
    );
  }
}
