import 'package:flutter/material.dart';
import 'package:future_builder_test/model/comment.dart';

class CommentViewInheritedWidget extends InheritedWidget {
  final List<Comment> commentsList;
  final Map<String, Function> funcs;
  final Future<void> refreshList;

  CommentViewInheritedWidget({
    Key key,
    Widget child,
    this.commentsList,
    this.funcs,
    this.refreshList,
  }) : super(
          key: key,
          child: child,
        );

  @override
  bool updateShouldNotify(CommentViewInheritedWidget old) {
    return (old.commentsList != commentsList);
  }

  static CommentViewInheritedWidget of(BuildContext context) {
    return context.inheritFromWidgetOfExactType(CommentViewInheritedWidget);
  }
}
