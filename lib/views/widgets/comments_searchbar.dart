import 'package:flutter/material.dart';

class CommentsSearchBar extends StatefulWidget {
  @override
  CommentsSearchBarState createState() => new CommentsSearchBarState();
}

class CommentsSearchBarState extends State<CommentsSearchBar> {
  TextEditingController searchController;
  String searchString;

  @override
  void initState() {
    super.initState();
    searchController = new TextEditingController();
    searchController.addListener(() => searchList(searchController.text));
  }

  void searchList(String text) {
    setState(() {
      searchString = text;
    });
  }

  @override
  Widget build(BuildContext context) {
    return Row(
      mainAxisAlignment: MainAxisAlignment.spaceEvenly,
      mainAxisSize: MainAxisSize.max,
      children: <Widget>[
        Icon(Icons.search),
        Flexible(
          child: TextFormField(
            keyboardType: TextInputType.text,
            controller: searchController,
          ),
        ),
      ],
    );
  }
}
