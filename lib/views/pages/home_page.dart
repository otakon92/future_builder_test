import 'dart:async';

import 'package:flutter/material.dart';
import 'package:future_builder_test/model/comment.dart';
import 'package:future_builder_test/utilities/dataproviders.dart';
import 'package:future_builder_test/views/widgets/comment_view_component.dart';
import 'package:future_builder_test/views/widgets/comment_view_inherited_widget.dart';

class MyHomePage extends StatefulWidget {
  MyHomePage({Key key, this.title}) : super(key: key);

  final String title;

  @override
  _MyHomePageState createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {
  Future<List<Comment>> commentBuilder;
  List<Comment> commentsList;
  TextEditingController nomeController;
  TextEditingController emailController;
  TextEditingController bodyController;
  Timer timer;

  @override
  void initState() {
    super.initState();
    commentsList = new List();
    commentBuilder = DataProviders.getAllComments();
    // timer = new Timer.periodic(
    //     Duration(seconds: 10), (Timer t) => refreshFunction());
  }

  void refreshList(){
    setState(() {
      commentBuilder = DataProviders.getAllComments();
    });
    // return null;
  }

  void showEditableDialog(BuildContext context, int id) {
    nomeController = new TextEditingController(
        text: commentsList.firstWhere((Comment c) => c.id == id).name);
    emailController = new TextEditingController(
        text: commentsList.firstWhere((Comment c) => c.id == id).email);
    bodyController = new TextEditingController(
        text: commentsList.firstWhere((Comment c) => c.id == id).body);
    showDialog(
        context: context,
        builder: (_) => SimpleDialog(
              title: Text("Modifica dettagli"),
              children: <Widget>[
                Column(
                  children: <Widget>[
                    Row(
                      children: <Widget>[
                        Text("Nome"),
                        Flexible(
                          child: TextFormField(
                            controller: nomeController,
                          ),
                        ),
                      ],
                    ),
                    Row(
                      children: <Widget>[
                        Text("Email"),
                        Flexible(
                          child: TextFormField(
                            controller: emailController,
                          ),
                        ),
                      ],
                    ),
                    Row(
                      children: <Widget>[
                        Text("Body"),
                        Flexible(
                          child: TextFormField(
                            controller: bodyController,
                          ),
                        ),
                      ],
                    ),
                  ],
                ),
                RaisedButton(
                  onPressed: () => changeDetail(context, id, {
                        "name": nomeController.text,
                        "email": emailController.text,
                        "body": bodyController.text
                      }),
                  child: Text("Modifica"),
                ),
              ],
            ));
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(widget.title),
      ),
      body: Center(
        child: FutureBuilder<List<Comment>>(
          future: commentBuilder,
          builder: (context, snapshot) {
            if (snapshot.connectionState == ConnectionState.done) {
              if (snapshot.hasData) {
                commentsList = snapshot.data;
              } else if (snapshot.hasError) {
                return Text("${snapshot.error}");
              }
            }
            return CommentViewInheritedWidget(
              commentsList: commentsList,
              funcs: {
                "showEditableDialog": this.showEditableDialog,
                "refreshList": this.refreshList,
              },
              child: CommentView(),
            );
          },
        ),
      ),
    );
  }

  @override
  void dispose() {
    super.dispose();
    // timer.cancel();
  }

  changeDetail(BuildContext context, int id, Map<String, String> fields) {
    setState(() {
      commentsList.firstWhere((Comment c) => c.id == id).body = fields["body"];
      commentsList.firstWhere((Comment c) => c.id == id).email =
          fields["email"];
      commentsList.firstWhere((Comment c) => c.id == id).name = fields["name"];
    });
    Navigator.of(context).pop();
  }
}
