class ConnectionData {
  static final String protocol = "http";
  static final String domain = "jsonplaceholder.typicode.com";

  static String connectionString(String contentType) {
    return protocol + "://" + domain + "/" + contentType;
  }
}
