import 'dart:convert';

import 'package:future_builder_test/model/comment.dart';
import 'package:future_builder_test/utilities/connection_data.dart';
import 'package:http/http.dart' as http;

class DataProviders {

  static Future<List<Comment>> getAllComments(){
     return http.get(ConnectionData.connectionString("comments")).then((dynamic response) {
       List<dynamic> parsedJson = json.decode(response.body);
       List<Comment> commentList = new List();
       for (Map<String, dynamic> item in parsedJson) {
         commentList.add(Comment.fromJson(item));
       }
       return commentList;
     });
  }
}