class Comment {
  int postId;
  int id;
  String name;
  String email;
  String body;

  Comment(this.postId, this.id, this.body, this.email, this.name);

  Comment.fromJson(Map<String, dynamic> parsedJson) {
    this.postId = parsedJson["postId"];
    this.id = parsedJson["id"];
    this.name = parsedJson["name"];
    this.email = parsedJson["email"];
    this.body = parsedJson["body"];
  }
}
