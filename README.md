# future_builder_test

An example Flutter project that refreshes an editable list every 10 seconds.

You can change the list fields, but the app will still refreshes them from the same source, so any change will be overridden.

The content is provided by [this API.](https://jsonplaceholder.typicode.com). 
